/*
 ============================================================================
 Name        : PicoLz
 Author      : Mazarei
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "lz.h"
#include "LZSS.h"
#include "md5.h"
#include "argparse.h"
#include "systimer.h"


#define 	MD5_DIGEST_LENGTH		16
FILE *infile, *outfile;

typedef enum
{
	LZ_Algorithm = 0xA0,
	LZSS_Algorithm
}Compress_Alguritm;

typedef struct
{
	unsigned char *srcbffer;
	uint32_t srcSize;
	uint32_t srcseek;
	FILE *dst;
}LZSSCallBack;

typedef struct
{
	unsigned char *srcbffer;
	unsigned char *outbuffer;
	uint32_t srcSize;
	uint32_t outSize;
}LZ77CallBack;

int  LZ_fWrite(void *UserParam,int offset, int Data)
{
	LZ77CallBack *IO = ((LZ77CallBack*)UserParam);
	if(IO->outSize > offset)
	{
		IO->outbuffer[offset] = Data;
		return 1;
	}
	return EOF;
}


int  LZ_fWriteReadBack(void *UserParam,int offset)
{
	LZ77CallBack *IO = ((LZ77CallBack*)UserParam);
	if(IO->outSize>offset)
	{
		return IO->outbuffer[offset];
	}
	return EOF;
}

int  LZ_fRead(void *UserParam,int offset)
{
	LZ77CallBack *IO = ((LZ77CallBack*)UserParam);
	if(IO->srcSize>offset)
	{
		return IO->srcbffer[offset];
	}
	return EOF;
}


int  LZSS_WriteTofile(void *UserParam, int Data)
{
	return fputc(Data,((LZSSCallBack*)UserParam)->dst);
}

int LZSS_ReadFromFile(void *UserParam)
{
	LZSSCallBack *IO = ((LZSSCallBack*)UserParam);
	if(IO->srcSize>IO->srcseek)
	{
		return IO->srcbffer[IO->srcseek++];
	}
	return EOF;
}




static const char *const usages[] =
{
    "\nPicoLz [options] SourceFile DestinationFile",
    NULL,
};

long GetFileSize( FILE *f )
{
    long pos, size;

    pos = ftell( f );
    fseek( f, 0, SEEK_END );
    size = ftell( f );
    fseek( f, pos, SEEK_SET );

    return size;
}


void Compress_File(const char*Source,const char *Dest,Compress_Alguritm Alg)
{
	FILE *src;
	FILE *dst;
	src = fopen( Source, "rb" );
	if( !src )
	{
	    printf( "Unable to open Source file \"%s\".\n", Source );
	    exit(0);
	}
	long srcsize = GetFileSize(src); /*Get Size of Source File*/

	unsigned char *srcfile = (unsigned char *) malloc( srcsize );
	if( !srcfile )
	{
	    printf( "Not enough memory\n" );
	    fclose( src );
	    exit(0);
	}

	fread( srcfile, srcsize, 1, src ); /*Load File in Ram*/
	fclose( src );

	/* Open output file */
	dst = fopen( Dest, "wb" );
	if( !dst )
	{
	   printf( "Unable to open output file \"%s\".\n", Dest );
	   free( srcfile );
	   exit(0);
	}

	/*Calc inputFile MD5*/
	MD5_CTX mdContext;
	unsigned char MD5Hash[MD5_DIGEST_LENGTH];
	MD5Init (&mdContext);
	MD5Update (&mdContext, srcfile, srcsize);
	MD5Final (MD5Hash,&mdContext);

	fwrite ("PicoLz0.1" , sizeof(char), sizeof("PicoLz0.1"), dst); /*Write Header Of App*/
	fwrite (&Alg , sizeof(char), 1, dst); /*Write Type of Compress Alguritm*/
	fwrite (&srcsize , sizeof(char), sizeof(uint32_t), dst); /*Write Size of Original File*/
	fwrite (MD5Hash , sizeof(char), MD5_DIGEST_LENGTH, dst); /*Write MD5 Hash Of Original File*/

	uint32_t outsize;

	if(Alg==LZSS_Algorithm)
	{
		LZSSIO_t LZFIO;
		unsigned char Lzss_WorkBuffer[Lzss_BufferSize()];
		LZSSCallBack CallBack;

		CallBack.dst = dst;
		CallBack.srcbffer = srcfile;
		CallBack.srcSize  = srcsize;
		CallBack.srcseek  = 0;


		 Lzss_Init(&LZFIO);
		 LZFIO.LZSS_Read = LZSS_ReadFromFile;
		 LZFIO.LZSS_Write = LZSS_WriteTofile;
		 LZFIO.UserParam = &CallBack;
		 LZFIO.buffer = Lzss_WorkBuffer;
		 printf("\r\nLzss Compression\r\n");
		 Lzss_Compression(&LZFIO);
	}
	else if(Alg==LZ_Algorithm)
	{
		unsigned int *work = malloc( sizeof(unsigned int) * (65536+srcsize) );
		outsize = (srcsize*104+50)/100 + 384;
		unsigned char *out = malloc( outsize );

		if( !out )
		{
		   printf( "Not enough memory For LZ77\n" );
		   fclose( dst );
		   free( srcfile );
		   exit(1);
		}

		printf("\r\nLz77 Compression\r\n");
		if( work )
		{
		   outsize = LZ_CompressFast( srcfile, out, srcsize, work );
		   free( work );
		}
		else
		{
		   outsize = LZ_Compress( srcfile, out, srcsize );
		}

		/* Write output file */
		  fwrite( out, outsize, 1, dst);
		  free( out );
		  printf("text:  %i bytes\r\n",srcsize);
		  printf("code:  %d bytes (%.1f%%)\n", outsize,
		                  100*(float)outsize/(float)srcsize );
	}
	else
	{
		printf( "Not Sport This Alguritm \"%i\".\n",  Alg);
		free( srcfile );
		fclose( dst );
		exit(0);
	}

	free( srcfile );
	fclose( dst );
}


void DeCompress_File(const char*Source,const char *Dest)
{
	FILE *src;
	FILE *dst;
	src = fopen( Source, "rb" );
	if( !src )
	{
	    printf( "Unable to open Source file \"%s\".\n", Source );
	    exit(0);
	}
	long srcsize = GetFileSize(src) - 0x1F/*Header Size*/; /*Get Size of Source File*/

	char HeaderFile[20];
	uint32_t UncompressSize;
	unsigned char MD5Hash[MD5_DIGEST_LENGTH];
	fread(HeaderFile,10,1,src);
	Compress_Alguritm Alg = (Compress_Alguritm) fgetc(src);
	fread(&UncompressSize,sizeof(uint32_t),1,src);
	fread(MD5Hash,MD5_DIGEST_LENGTH,1,src);

	if(!strstr(HeaderFile,"PicoLz") || (Alg!=LZ_Algorithm && Alg!=LZSS_Algorithm))
	{
		printf( "Not Sport This File\n" );
		fclose(src);
		exit(0);
	}
	printf("\r\nFile Version: %s\r\n",HeaderFile);
	printf("DeCompress Algorithm : %s\r\n",(Alg==LZ_Algorithm) ? "LZ Algorithm":"LZSS Algorithm");


	unsigned char *srcfile = (unsigned char *) malloc(srcsize);
	if( !srcfile )
	{
	    printf( "Not enough memory\n" );
	    fclose( src );
	    exit(0);
	}

	fread( srcfile, srcsize, 1, src ); /*Load File in Ram*/
	fclose( src );

	/* Open output file */
	dst = fopen( Dest, "wb" );
	if( !dst )
	{
	   printf( "Unable to open output file \"%s\".\n", Dest );
	   free( srcfile );
	   exit(0);
	}

	double        t0,t1;

	if(Alg==LZSS_Algorithm)
	{
		LZSSIO_t LZFIO;
		unsigned char Lzss_WorkBuffer[Lzss_BufferSize()];
		LZSSCallBack CallBack;

		CallBack.dst = dst;
		CallBack.srcbffer = srcfile;
		CallBack.srcSize  = srcsize;
		CallBack.srcseek  = 0;



		 Lzss_Init(&LZFIO);
		 LZFIO.LZSS_Read = LZSS_ReadFromFile;
		 LZFIO.LZSS_Write = LZSS_WriteTofile;
		 LZFIO.UserParam = &CallBack;
		 LZFIO.buffer = Lzss_WorkBuffer;
		 printf("\r\nLzss Decompression\r\n");
		 t0 = GetTime();
		 Lzss_Decompression(&LZFIO);
		 t1 = GetTime();
	}
	else if(Alg==LZ_Algorithm)
	{
		LZ77IO_t LZIO;
		LZ77CallBack LZCallBack;

		LZCallBack.outSize = UncompressSize;
		LZCallBack.outbuffer = malloc(UncompressSize);
		LZCallBack.srcSize = srcsize;
		LZCallBack.srcbffer = srcfile;

		if( !LZCallBack.outbuffer )
		{
			printf( "Not enough memory\n" );
			free( srcfile );
			fclose( dst );
			exit(0);
		}

		LZIO.insize = srcsize;
		LZIO.LZ_Read = LZ_fRead;
		LZIO.LZ_Write= LZ_fWrite;
		LZIO.LZ_WriteReadBack = LZ_fWriteReadBack;
		LZIO.UserParam = &LZCallBack;
		t0 = GetTime();
		LZ_Uncompress (&LZIO);
		fwrite(LZCallBack.outbuffer,UncompressSize,1,dst);
		t1 = GetTime();
		free(LZCallBack.outbuffer);

	}
	else
	{
		printf( "Not Sport This Alguritm \"%i\".\n",  Alg);
		free( srcfile );
		fclose( dst );
		exit(0);
	}

	free( srcfile );
	fclose( dst );

	/*Check Output File Validate*/
	src = fopen( Dest, "rb" );
	long Outsize = GetFileSize(src);
	srcfile = (unsigned char *) malloc(Outsize);
	if( !srcfile )
	{
	    printf( "Not enough memory\n" );
	    fclose( src );
	    exit(0);
	}

	fread( srcfile, Outsize, 1, src ); /*Load File in Ram*/
	fclose( src );
	/*Calc Output File MD5*/
	MD5_CTX mdContext;
	unsigned char FileMD5Hash[MD5_DIGEST_LENGTH];
	MD5Init (&mdContext);
	MD5Update (&mdContext, srcfile, Outsize);
	MD5Final (FileMD5Hash,&mdContext);
	free(srcfile);

	if(Outsize == UncompressSize)
	{
		printf("Decompress File Size ok : %i Byte\r\n",Outsize);
	}
	else
	{
		printf("Error Decompress File.\r\n File Size Not Match\r\n");
		exit(0);
	}

	if(memcmp(FileMD5Hash,MD5Hash,MD5_DIGEST_LENGTH)==0)
	{
		printf("Decompress MD5 Match OK\r\nMD5:%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X\r\n",
				MD5Hash[0],MD5Hash[1],MD5Hash[2],MD5Hash[3],MD5Hash[4],MD5Hash[5],MD5Hash[6],MD5Hash[7],
				MD5Hash[8],MD5Hash[9],MD5Hash[10],MD5Hash[11],MD5Hash[12],MD5Hash[13],MD5Hash[14],MD5Hash[15]);
		printf("Uncompression speed: %.1f KB/s (%.2f ms)\n",(double) Outsize / (1024.0 * (t1-t0)), 1000.0 * (t1-t0));
	}
	else
	{
		printf("Error Decompress File.\r\n File MD5 Not Match\r\n");
		exit(0);
	}

}




int main(int argc, const char **argv)
{

    int compress = 0;
    int decompress = 0;
    int lz = 0;
    int lzss = 0;
    const char *SourcePath = NULL;
    const char *DestinationFile = NULL;

	struct argparse_option options[] =
	{
	    OPT_HELP(),
	    OPT_GROUP("Basic options"),
	    OPT_BOOLEAN('c', "compress", &compress, "Compression SourceFile"),
	    OPT_BOOLEAN('d', "decompress", &decompress, "DeCompression SourceFile"),
		OPT_BOOLEAN('l', "lz", &lz, "Use lz77 Algorithm For Compression"),
		OPT_BOOLEAN('s', "lzss", &lzss, "Use lzss Algorithm For Compression"),
	    OPT_END(),
	};

	printf("\r\nPicoLz Write By Mohammad Mazarei\r\n This Soft Use Lz Algorithm For Compress And DeCompress File\r\n");
	struct argparse argparse;
	    argparse_init(&argparse, options, usages, 0);
	    argc = argparse_parse(&argparse, argc, argv);

	    SourcePath = argv[argc-2];
	    DestinationFile = argv[argc-1];

	    if(argc<2 || DestinationFile==NULL || SourcePath==NULL)
	    {
	    	printf("\r\nRead Help : picolz -h\r\n");
	    	return 0;
	    }

	    if((compress==0 && decompress==0) || (compress==1 && decompress==1))
	    {
	    	printf("\r\nNot Valid options.\r\n Read Help.");
	    	return 0;
	    }

	    if((lz==0 && lzss==0) || (lz==1 && lzss==1))
	    {
	    	printf("\r\n Not Valid options.\r\n Read Help.");
	    	return 0;
	    }

	if(compress)
	{
		 Compress_File(SourcePath,DestinationFile,(lz) ? LZ_Algorithm:LZSS_Algorithm);
	}
	else if(decompress)
	{
		DeCompress_File(SourcePath,DestinationFile);
	}

	return EXIT_SUCCESS;
}
