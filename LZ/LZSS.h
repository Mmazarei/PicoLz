/*
 * LZSS.h
 *
 *  Created on: Jul 28, 2016
 *      Author: Mazarei
 */

#ifndef LZSS_H_
#define LZSS_H_

#define EI  13  /* typically 8..13 */
#define EJ  6  /* typically 4..5 */
#define P   1  /* If match length <= P then output one character */
#define N (1 << EI)  /* buffer size */
#define F ((1 << EJ) + 1)  /* lookahead buffer size */



typedef struct
{
	int  (*LZSS_Write)(void *UserParam, int Data);
	int (*LZSS_Read)(void *UserParam);
	void *UserParam;

	unsigned char *buffer; /*Work Buffer*/
	int bit_buffer;
	int bit_mask;
	unsigned long codecount;
	unsigned long textcount;
}LZSSIO_t;

#define Lzss_BufferSize()	(N * 2)
void Lzss_Init(LZSSIO_t *LZIO);
void Lzss_Compression(LZSSIO_t *LZIO);
void Lzss_Decompression(LZSSIO_t *LZIO);


#endif /* LZSS_H_ */
