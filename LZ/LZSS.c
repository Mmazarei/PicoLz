/*
 * LZSS.c
 *
 *  Created on: Jul 28, 2016
 *      Author: Mazarei
 */

/* LZSS encoder-decoder (Haruhiko Okumura; public domain) */

#include "../LZ/LZSS.h"

#include <stdio.h>
#include <stdlib.h>


void Lzss_Init(LZSSIO_t *LZIO)
{
	LZIO->bit_buffer = 0;
	LZIO->bit_mask   = 128;
	LZIO->codecount  = 0;
	LZIO->textcount  = 0;
}

void error(void)
{
    printf("Output error\n");  exit(1);
}

void putbit1(LZSSIO_t *LZIO)
{
	LZIO->bit_buffer |= LZIO->bit_mask;
    if ((LZIO->bit_mask >>= 1) == 0)
    {
        if (LZIO->LZSS_Write(LZIO->UserParam, LZIO->bit_buffer) == EOF)
        	error();

        LZIO->bit_buffer = 0;
        LZIO->bit_mask = 128;
        LZIO->codecount++;
    }
}

void putbit0(LZSSIO_t *LZIO)
{
    if ((LZIO->bit_mask >>= 1) == 0)
    {
        if (LZIO->LZSS_Write(LZIO->UserParam, LZIO->bit_buffer) == EOF)
        	error();

        LZIO->bit_buffer = 0;
        LZIO->bit_mask = 128;
        LZIO->codecount++;
    }
}

void flush_bit_buffer(LZSSIO_t *LZIO)
{
    if (LZIO->bit_mask != 128)
    {
        if (LZIO->LZSS_Write(LZIO->UserParam, LZIO->bit_buffer) == EOF)
        		error();
        LZIO->codecount++;
    }
}

void output1(LZSSIO_t *LZIO,int c)
{
    int mask;

    putbit1(LZIO);
    mask = 256;
    while (mask >>= 1) {
        if (c & mask) putbit1(LZIO);
        else putbit0(LZIO);
    }
}

void output2(LZSSIO_t *LZIO,int x, int y)
{
    int mask;

    putbit0(LZIO);
    mask = N;
    while (mask >>= 1)
    {
        if (x & mask)
        	putbit1(LZIO);
        else
        	putbit0(LZIO);
    }

    mask = (1 << EJ);
    while (mask >>= 1)
    {
        if (y & mask)
        	putbit1(LZIO);
        else
        	putbit0(LZIO);
    }
}

void Lzss_Compression(LZSSIO_t *LZIO)
{
    int i, j, f1, x, y, r, s, bufferend, c;

    for (i = 0; i < N - F; i++)
    	LZIO->buffer[i] = ' ';

    for (i = N - F; i < N * 2; i++)
    {
        if ((c = LZIO->LZSS_Read(LZIO->UserParam)) == EOF)
        		break;

        LZIO->buffer[i] = c;
        LZIO->textcount++;
    }

    bufferend = i;
    r = N - F;
    s = 0;

    while (r < bufferend)
    {
        f1 = (F <= bufferend - r) ? F : bufferend - r;
        x = 0;
        y = 1;
        c = LZIO->buffer[r];

        for (i = r - 1; i >= s; i--)
        {
            if (LZIO->buffer[i] == c)
            {
                for (j = 1; j < f1; j++)
                    if (LZIO->buffer[i + j] != LZIO->buffer[r + j])
                    	break;

                if (j > y)
                {
                    x = i;  y = j;
                }
            }
        }

        if (y <= P)
       	{
        	y = 1;
        	output1(LZIO,c);
        }
        else
        {
        	output2(LZIO,x & (N - 1), y - 2);
        }

        r += y;
        s += y;

        if (r >= N * 2 - F)
        {
            for (i = 0; i < N; i++)
            {
            	LZIO->buffer[i] = LZIO->buffer[i + N];
            }

            bufferend -= N;
            r -= N;
            s -= N;
            while (bufferend < N * 2)
            {
                if ((c = LZIO->LZSS_Read(LZIO->UserParam)) == EOF)
                	break;
                LZIO->buffer[bufferend++] = c;
                LZIO->textcount++;
            }
        }
    }

    flush_bit_buffer(LZIO);
    printf("text:  %ld bytes\n", LZIO->textcount);
    printf("code:  %ld bytes (%ld%%)\n",
    		LZIO->codecount, (LZIO->codecount * 100) /LZIO->textcount);
}

int getbit(LZSSIO_t *LZIO , int n) /* get n bits */
{
    int i, x;
    static int buf, mask = 0;

    x = 0;
    for (i = 0; i < n; i++)
    {
        if (mask == 0)
        {
            if ((buf = LZIO->LZSS_Read(LZIO->UserParam)) == EOF) return EOF;
            mask = 128;
        }

        x <<= 1;
        if (buf & mask)
        	x++;
        mask >>= 1;
    }

    return x;
}

void Lzss_Decompression(LZSSIO_t *LZIO)
{
    int i, j, k, r, c;

    for (i = 0; i < N - F; i++)
    	LZIO->buffer[i] = ' ';
    r = N - F;

    while ((c = getbit(LZIO,1)) != EOF)
    {
        if (c)
        {
            if ((c = getbit(LZIO,8)) == EOF) break;
            LZIO->LZSS_Write(LZIO->UserParam,c);
            LZIO->buffer[r++] = c;  r &= (N - 1);
        }
        else
        {
            if ((i = getbit(LZIO,EI)) == EOF) break;
            if ((j = getbit(LZIO,EJ)) == EOF) break;
            for (k = 0; k <= j + 1; k++)
            {
                c = LZIO->buffer[(i + k) & (N - 1)];
                LZIO->LZSS_Write(LZIO->UserParam,c);
                LZIO->buffer[r++] = c;  r &= (N - 1);
            }
        }
    }
}
