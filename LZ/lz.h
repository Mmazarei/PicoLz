/*************************************************************************
* Name:        lz.h
* Author:      Marcus Geelnard
* Changed By   Mohammad Mazarei For Steaming UnComtress
* Description: LZ77 coder/decoder interface.
* Reentrant:   Yes
*-------------------------------------------------------------------------
* Copyright (c) 2003-2006 Marcus Geelnard
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, subject to the following restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
*    claim that you wrote the original software. If you use this software
*    in a product, an acknowledgment in the product documentation would
*    be appreciated but is not required.
*
* 2. Altered source versions must be plainly marked as such, and must not
*    be misrepresented as being the original software.
*
* 3. This notice may not be removed or altered from any source
*    distribution.
*
* Marcus Geelnard
* marcus.geelnard at home.se
*************************************************************************/

#ifndef _lz_h_
#define _lz_h_

#ifdef __cplusplus
extern "C" {
#endif


/*************************************************************************
* Function prototypes
*************************************************************************/

typedef struct
{
	int  (*LZ_Write)(void *UserParam,int offset, int Data); /*Write To Output File*/
	int  (*LZ_WriteReadBack)(void *UserParam,int offset);   /*Read Form OutPut File*/
	int  (*LZ_Read)(void *UserParam,int offset);			/*Read From Source File*/
	unsigned int insize;

	void *UserParam;

}LZ77IO_t;


int LZ_Compress( unsigned char *in, unsigned char *out,
                 unsigned int insize );
int LZ_CompressFast( unsigned char *in, unsigned char *out,
                     unsigned int insize, unsigned int *work );

void LZ_Uncompress( LZ77IO_t *IO );


#ifdef __cplusplus
}
#endif

#endif /* _lz_h_ */
